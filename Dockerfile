FROM node:12.13-alpine

ENV HOME=/home

COPY package.json $HOME/app/

WORKDIR $HOME/app

RUN npm install --quiet

COPY . $HOME/app/

# RUN npm run build

# RUN npm prune --production